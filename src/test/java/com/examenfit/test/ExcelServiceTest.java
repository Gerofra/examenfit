package com.examenfit.test;


import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.examenfit.entities.Empresa;
import com.examenfit.entities.Movimiento;
import com.examenfit.services.ExcelService;

@SpringBootTest
class ExcelServiceTest {

	@Autowired
	ExcelService excelService;
	
	private List<Empresa> empresas = new ArrayList<>();
	
	@BeforeEach
	void setUp() throws Exception {				
		Empresa empresa = new Empresa();
		empresa.setNroContrato(1);
		empresa.setCodigoPostal(1111);
		empresa.setCiiu("2222");
		empresa.setCuit("3333");
		empresa.setDenominacion("denom");
		empresa.setDomicilio("Calle A 123");
		empresa.setOrganizador("org");
		empresa.setProductor("prod");
		
		List<Movimiento> movimientos = new ArrayList<>();
		Movimiento movimiento = new Movimiento();
		movimiento.setNroContrato(1);
		movimiento.setSaldoCtaCte(2.00);
		movimiento.setConcepto("concepto");
		movimiento.setImporte(2.00);
		movimiento.setTipo("tipo");
		
		empresa.setMovimientos(movimientos);
		empresas.add(empresa);
	}
	

	@Test
	void testResponseValidarXml() throws Exception {	
		
		assertEquals(empresas, excelService.validarXml(empresas));
	}
	
	
	@Test
	void testExceptionValidarXml() throws Exception {

		empresas.get(0).setNroContrato(null);	
		assertThrows(Exception.class, () ->  excelService.validarXml(empresas));
	}

}
